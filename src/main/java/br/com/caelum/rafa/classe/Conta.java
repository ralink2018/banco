package br.com.caelum.rafa.classe;

public class Conta {
	
	private double saldo;
	
	public void deposita(double valor){
		if(valor < 0){
			throw new IllegalArgumentException();
		}
		this.saldo += valor;
	}
	public void sacar(double valor){
		if(saldo <= 0){
			throw new IllegalArgumentException();
		}
		this.saldo -= valor;
	}

	public double getSaldo() {
		return saldo;
	}
	

}
