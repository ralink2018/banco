package br.com.caelum.rafa.classe;

import org.junit.Assert;
import org.junit.Test;

public class ContaTest {

	@Test
	public void deve_depositar_valor_em_conta() {
		Conta conta = new Conta();
		double valorDeposito = 100;
		conta.deposita(valorDeposito);
		Assert.assertEquals(100, conta.getSaldo(), 0.001);
	}

	@Test
	public void deve_manter_conta_zerada() {
		Conta conta = new Conta();
		conta.deposita(0);
		Assert.assertEquals(0, conta.getSaldo(), 0.001);
	}
	
	@Test
	public void deve_fazer_multiplos_depositos(){
		Conta c = new Conta();
		c.deposita(100);
		c.deposita(200);
		
		Assert.assertEquals(300, c.getSaldo(), 0.0001);
	}

	@Test(expected = IllegalArgumentException.class)
	public void Nao_Negativar_Saldo() {
		Conta conta = new Conta();
		conta.deposita(-1);
	}
	@Test
	public void deve_fazer_um_saque_quando_tiver_saldo(){
		Conta conta = new Conta();
		conta.deposita(100);
		conta.sacar(1);
		Assert.assertEquals(99, conta.getSaldo(), 0.0001);
	
	}
	@Test(expected=IllegalArgumentException.class)
	public void nao_deve_fazer_saque_quando_nao_tiver_saldo(){
		Conta conta = new Conta();
		//conta.deposita(5);
		conta.sacar(10);
		Assert.assertEquals(0, conta.getSaldo(), 0.0001);
	}
}
